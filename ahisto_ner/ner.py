from logging import getLogger

from .util import run_docker_container


LOGGER = getLogger(__name__)


def tag_texts(client, input_volume, input_filename: str, output_volume, output_filename: str, gpus: str) -> None:
    LOGGER.info(f'Running ahisto-ner tag-texts {input_volume.short_id}/{input_filename} to {output_volume.short_id}/{output_filename}')

    volumes = {
        input_volume.name: {
            'bind': '/input',
            'mode': 'ro',
        },
        output_volume.name: {
            'bind': '/output',
            'mode': 'rw',
        },
    }

    command = ['tag-texts', f'/input/{input_filename}', f'/output/{output_filename}']

    run_docker_container(client, 'gitlab.fi.muni.cz:5050/nlp/ahisto-modules/ner-eval', runtime='nvidia',
                         environment={'NVIDIA_VISIBLE_DEVICES': gpus},
                         command=command, volumes=volumes)
