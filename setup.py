from setuptools import setup

setup(
    name='ahisto-ner',
    author='Vít Novotný',
    author_email='witiko@mail.muni.cz',
    url='https://gitlab.fi.muni.cz/nlp/ahisto-modules/ner',
    version='1.0.1',
    entry_points={
        'console_scripts': [
            'ahisto-ner=ahisto_ner.cli:cli',
        ],
    },
    setup_requires=[
        'setuptools',
    ],
    description='The NER tool of the AHISTO project',
    packages=[
        'ahisto_ner',
    ],
    package_dir={
        'ahisto_ner': 'ahisto_ner',
    },
    include_package_data=True,
    zip_safe=True,
    install_requires=[
        'docker~=6.1.1',
        'click~=7.1.2',
        'pyxdg~=0.26',
    ],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',
    ],
)
