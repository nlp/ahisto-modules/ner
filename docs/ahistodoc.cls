\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{ahistodoc}
\LoadClass[oneside]{book}
% Typography
\RequirePackage{microtype}
\RequirePackage{fontspec}
\defaultfontfeatures{Scale=MatchLowercase}
\setmainfont{EB Garamond}
\setmonofont{InconsolataN}
\RequirePackage{emptypage}
\RequirePackage{minted}
\setminted{breaklines,fontsize=\small}
% Graphics
\RequirePackage{graphicx}
% Markup
\RequirePackage[theme=witiko/ahisto/doc]{markdown}
