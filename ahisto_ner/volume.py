from pathlib import Path
from logging import getLogger
from shutil import copyfileobj
from tempfile import TemporaryFile
import tarfile

from .util import create_temporary_docker_container


LOGGER = getLogger(__name__)


def copy_input_to(client, volume, input_file: Path, filename: str) -> None:
    LOGGER.info(f'Copying file {input_file} to Docker volume {volume.short_id}, file {filename}')
    volumes = {volume.name: {'bind': '/input', 'mode': 'rw'}}
    with create_temporary_docker_container(client, 'gitlab.fi.muni.cz:5050/nlp/ahisto-modules/empty', command='cmd',
                                           volumes=volumes) as container:
        with TemporaryFile('w+b') as tf:
            with tarfile.open(fileobj=tf, mode='w') as tar:
                tar.add(input_file, filename)
            tf.seek(0)
            container.put_archive(path='/input', data=tf)


def copy_output_from(client, volume, filename: str, output_file: Path) -> None:
    LOGGER.info(f'Copying file {output_file} from Docker volume {volume.short_id}, file {filename}')
    volumes = {volume.name: {'bind': '/output', 'mode': 'ro'}}
    with create_temporary_docker_container(client, 'gitlab.fi.muni.cz:5050/nlp/ahisto-modules/empty', command='cmd',
                                           volumes=volumes) as container:
        with TemporaryFile('w+b') as tf:
            for chunk in container.get_archive('/output')[0]:
                tf.write(chunk)
            tf.seek(0)
            with tarfile.open(fileobj=tf, mode='r') as tar:
                fsrc = tar.extractfile(f'output/{filename}')
                assert fsrc is not None
                with output_file.open('wb') as fdst:
                    copyfileobj(fsrc, fdst)
