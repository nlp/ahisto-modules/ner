import logging
from logging import getLogger
from pathlib import Path

from .config import CONFIG as _CONFIG
from .classification import classify_texts as _classify_texts
from .ner import tag_texts as _tag_texts
from .util import create_temporary_docker_volume, detect_free_gpu
from .volume import copy_input_to, copy_output_from

import click
import docker


LOGGER = getLogger(__name__)
CONFIG = _CONFIG['cli']


@click.group()
@click.pass_context
def cli(context: click.Context):
    logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')

    client = docker.from_env()

    def check_docker_image(name: str) -> None:
        if not client.images.list(name):
            LOGGER.info(f'Pulling image {name}')
            client.images.pull(name)

    check_docker_image('gitlab.fi.muni.cz:5050/nlp/ahisto-modules/empty')
    check_docker_image('gitlab.fi.muni.cz:5050/nlp/ahisto-modules/ner-eval')

    context.ensure_object(dict)
    context.obj['client'] = client


@cli.command()
@click.argument('input-file',
                type=click.Path(exists=True, dir_okay=False, file_okay=True),
                required=True)
@click.argument('output-file',
                type=click.Path(exists=None, dir_okay=False, file_okay=True),
                required=True)
@click.option('--gpus',
              help=('Either comma-separated PCI BUS IDs of NVIDIA GPUs that will be used for image super-resolution '
                    'and PERO OCR, `auto` for an automatic selection of a free GPU, or `all` for the use of all GPUs'),
              default=CONFIG['gpus'],
              required=False)
@click.pass_context
def tag_texts(context: click.Context, input_file: str, output_file: str, gpus: str) -> None:
    """For every input line with a text, produce an output line of NER tags for each word in the text."""

    client = context.obj['client']

    input_file = Path(input_file)
    output_file = Path(output_file)

    if gpus == 'auto':
        gpus = detect_free_gpu()

    with create_temporary_docker_volume(client) as input_volume, \
            create_temporary_docker_volume(client) as output_volume:

        copy_input_to(client, input_volume, input_file, 'input.txt')
        _tag_texts(client, input_volume, 'input.txt', output_volume, 'output.txt', gpus)
        copy_output_from(client, output_volume, 'output.txt', output_file)


@cli.command()
@click.argument('input-texts-file',
                type=click.Path(exists=True, dir_okay=False, file_okay=True),
                required=True)
@click.argument('input-ner-tags-file',
                type=click.Path(exists=True, dir_okay=False, file_okay=True),
                required=True)
@click.argument('output-file',
                type=click.Path(exists=None, dir_okay=False, file_okay=True),
                required=True)
@click.pass_context
def classify_texts(context: click.Context, input_texts_file: str, input_ner_tags_file: str, output_file: str) -> None:
    """For every input line with a text, produce an output line with a prediction whether the text comes from index."""

    client = context.obj['client']

    input_texts_file = Path(input_texts_file)
    input_ner_tags_file = Path(input_ner_tags_file)
    output_file = Path(output_file)

    with create_temporary_docker_volume(client) as input_volume, \
            create_temporary_docker_volume(client) as output_volume:

        copy_input_to(client, input_volume, input_texts_file, 'input-texts.txt')
        copy_input_to(client, input_volume, input_ner_tags_file, 'input-ner-tags.txt')
        _classify_texts(client, input_volume, 'input-texts.txt', 'input-ner-tags.txt', output_volume, 'output.txt')
        copy_output_from(client, output_volume, 'output.txt', output_file)
