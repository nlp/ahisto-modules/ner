This chapter lists the use cases detected as a result in the functional requirement analysis of the AHISTO NER tool and the associated software, see also Figure <#use-case-diagram>.

# Named Entity Recognition

- recognize named entities in a paragraph of text

# Named Entity Postprocessing

- decide whether a named entity should be displayed on the AHISTO portal
- convert a named entity to a form that will be displayed on the AHISTO portal

 /use-case-diagram.tex