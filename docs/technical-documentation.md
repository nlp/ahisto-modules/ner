This chapter contains the technical documentation of the AHISTO NER tool and the associated software that you can find in the `zdrojovy_kod.zip`, `jazykove_modely.zip`, and `docker_obrazy.zip` archives.

# Design

In our software, we used the object-oriented programming paradigm to separate the implementation into separate modules that correspond to the individual responsibilities identified by requirement analysis. As our programming language, we used Python 3.8 and third-party software libraries documented in files `ner/requirements.txt` and `ner-eval/requirements.txt`. To package our software and trained named entity recognition models, we used Docker.

# Architecture

Here are the modules of our tool and their responsibilities, see also Figure <#package-diagram>:

- `ner-eval.entity`: Read named entities from abstracts of medieval deeds.
- `ner-eval.document`: Read OCR texts of books describing medieval deeds.
- `ner-eval.search` and `index`: Find entities from abstracts in books. [@novotny2023people]
- `ner-eval.recognition`: Train recognition models using found entities. [@novotny2023people]
- `ner-eval.cli` and `ner.cli`: Find entities in new texts using trained models.
- `ner-eval.classification`, `ner-postprocessing`, and `entity-declension`: Postprocess named entities before they are displayed on the AHISTO portal.

 /package-diagram.tex

Here are the components of our tool and how they interact, see also Figure <#component-diagram>:

- The `ahisto/ner` Docker image contains the `ner-eval` module and the trained named entity recognition model `MU-NLPC/ahisto-ner-model-s`.
  This Docker image is available at `gitlab.fi.muni.cz:5050/nlp/ahisto-modules/ner-eval`.
- The `ner` module provides a command-line interface to the `ahisto/ner` image.
- The `entity-declension` module provides a standalone command-line interface.

 /component-diagram.tex
