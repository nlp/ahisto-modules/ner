from logging import getLogger

from .util import run_docker_container


LOGGER = getLogger(__name__)


def classify_texts(client, input_volume, input_texts_filename: str, input_ner_tags_filename: str, output_volume,
                   output_filename: str) -> None:
    LOGGER.info(f'Running ahisto-ner classify-texts {input_volume.short_id}/{input_texts_filename} '
                f'{input_volume.short_id}/{input_ner_tags_filename} to {output_volume.short_id}/{output_filename}')

    volumes = {
        input_volume.name: {
            'bind': '/input',
            'mode': 'ro',
        },
        output_volume.name: {
            'bind': '/output',
            'mode': 'rw',
        },
    }

    command = ['classify-texts', f'/input/{input_texts_filename}', f'/input/{input_ner_tags_filename}', f'/output/{output_filename}']

    run_docker_container(client, 'gitlab.fi.muni.cz:5050/nlp/ahisto-modules/ner-eval', command=command, volumes=volumes)
