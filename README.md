`ahisto-ner` is a command-line tool for the named entity recognition in medieval texts, which was developed as a part of [the AHISTO TACR Eta grant project][4], number TL03000365.

## Installation

To install the tool, perform the following steps on a UNIX-based operating system:

1. [Install Python][1], [Docker][2], and [NVIDIA Container Toolkit][5].
2. [Create and activate a Python virtualenv][3] and run `make install` from the command line to install the tool.

## Usage

Create a file `texts.txt` with line-delimited medieval texts:

    Jan Žižka z Trocnova a Kalicha byl český husitský vojevůdce.
    Kundrát, bratři z Miroslavi, na základě svolení od probošta dolnokounického kláštera Jana a převorky, že mohou rozšířit rybník v Hlavaticích, slibují jen na jistou vzdálenost od šumického dvora zatopit a dovolit šumickým lidem, aby užívali tamní potok.
    Johannis Rupolth vac., ad present. nobilis Hinconis Berka de Duba residentis in castro Scharffstein. Exec. pleb. in Arnorssdorff. C, IIII.- Horzielicz.- Anno quo supra die XXVI April. data e. crida Thome, clerico de Antiqua Boleslauia, ad eccl. paroch.
    September 3. Der Rat zu Löbau leiht 160 Schock zum Bau und zur Besserung der durch Brand und die Ketzer zerstörten Stadt. Nach Knothe Urkundenbuch von Kamenz und Löbau S. 253 (nach dem Original im Löbaner Stadtarchiv, jetzt im Hauptstaatsarchiv zu Dresden). 25 1432. September 12. Item Nickel Windischs ist ufgenomen des freitags vor des heiligen creucis exaltation.

Use the `ahisto-ner` tool to recognize all entities in a set of line-delimited texts by running the following command from the command line:

    ahisto-ner tag-texts texts.txt ner_tags.txt

Here is example output of the tool:

    2023-02-13 14:23:02,618 Automatically selected "Tesla T4" NVIDIA GPU (PCI BUS ID: 10, 14907M free VRAM)
    2023-02-13 14:23:02,625 Copying file texts.txt to Docker volume d560e7bb44, file input.txt
    2023-02-13 14:23:02,734 Running ahisto-ner tag-texts d560e7bb44/input.txt 2028be6896/output.txt
    2023-02-13 14:23:19,795 Copying file ner_tags.txt from Docker volume 2028be6896, file output.txt

After running the above command, the file `ner_tags.txt` will be created and contain the recognized entities:

    B-PER I-PER I-PER B-LOC O B-LOC O O O O
    B-PER O O B-LOC O O O O O O O B-PER O O O O O O O B-LOC O O O O O O O O O O O O O O O O O
    B-PER I-PER O O O O B-PER I-PER I-PER I-PER I-PER I-PER I-PER I-PER O O O B-LOC O O B-LOC O O O O O O O O O B-PER O O B-LOC I-LOC O O O
    O O O O O B-LOC O O O O O O O O O O B-PER O O O O O O O O O B-LOC O B-LOC O O O O O O B-LOC I-LOC O O O O B-LOC O O O O B-PER I-PER I-PER O O O O O O O O O

Specifically, for each space-separated word in the input texts, a [BIO tag][6] is produced.

Run `ahisto-ner --help` from the command line for more information about the `tag-texts` command and other available commands.

 [1]: https://www.python.org/downloads/
 [2]: https://docs.docker.com/engine/install/
 [3]: https://docs.python.org/3/library/venv.html
 [4]: https://starfos.tacr.cz/en/project/TL03000365
 [5]: https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#docker
 [6]: https://en.wikipedia.org/wiki/Inside–outside–beginning_(tagging)
