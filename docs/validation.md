The AHISTO NER tool was validated by evaluation of several different methods for named entity recognition. The resulting Precision and Recall of the AHISTO NER tool are 72.81--93.98% and 58.14--81.77%, respectively. Details of the evaluation are available in the article of @novotny2023people. Evaluation protocols are enclosed in the `protokol_evaluace.zip` archive:

- `01_evaluate_different_methods.pdf`: select a method for producing a dataset
- `02_find_all_entities.pdf`, `04_extract_dataset_statistics.pdf`: produce a dataset for training named entity recognition models
- `03_train_ner_models.pdf`, `05_train_additional_ner_models_for_paper.pdf`: train and evaluate named entity recognition models